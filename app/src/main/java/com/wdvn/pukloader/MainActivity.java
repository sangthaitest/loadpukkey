package com.wdvn.pukloader;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.pax.dal.entity.PukInfo;
import com.wdvn.pukloader.api.TCPClient;
import com.wdvn.pukloader.api.models.IpData;
import com.wdvn.pukloader.api.models.KeyRequest;
import com.wdvn.pukloader.api.models.KeyResponse;
import com.wdvn.pukloader.api.models.LogonRequest;
import com.wdvn.pukloader.api.models.LogonResponse;
import com.wdvn.pukloader.api.models.RequestData;
import com.wdvn.pukloader.base.BaseApplication;
import com.wdvn.pukloader.interfaces.OnProcessApp;
import com.wdvn.pukloader.utils.Constants;
import com.wdvn.pukloader.utils.EncryptUtils;
import com.wdvn.pukloader.utils.JsonUtils;
import com.wdvn.pukloader.utils.PaxApi;
import com.wdvn.pukloader.utils.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.ed_ip)
    EditText edIp;
    @BindView(R.id.ed_port)
    EditText edPort;
    @BindView(R.id.ed_position)
    EditText edPosition;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.iv_success)
    ImageView ivSuccess;
    @BindView(R.id.iv_fail)
    ImageView ivFail;

    private static final int REQUEST_PERMISSION = 6666;

    private byte[] key;

    private String enKey;
    private int newPort = 0;
    private TCPClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();
        if (!checkPermission()) {
            requestPermission();
        } else {
            readInfoFromFile();
        }
    }

    private void readInfoFromFile() {
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this,
//                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
//        }
        String ipDataString = Util.readFromFile();
//        Log.d("Sang", "" + ipDataString);
        Log.d("Sang", "writePUKLoading " + BuildConfig.FLAVOR);
        if (!TextUtils.isEmpty(ipDataString)) {
            IpData ipData = new Gson().fromJson(ipDataString, IpData.class);
//            Log.d("Sang", "" + ipData.ip);
            edIp.setText(ipData.ip);
            edIp.setEnabled(false);
            edPort.setText(ipData.port + "");
            edPort.setEnabled(false);
            edPosition.setText(ipData.pos + "");
            edPosition.setEnabled(false);
        }
    }

    private void init() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        BaseApplication.getProcessLogObs()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                               @Override
                               public void accept(String log) throws Exception {
                                   tvError.setText(log);
                               }
                           }, new Consumer<Throwable>() {
                               @Override
                               public void accept(Throwable throwable) throws Exception {
                                   BaseApplication.appendProcessLog(throwable.getMessage());
                               }
                           }
                );
        SystemClock.sleep(100);
        BaseApplication.appendProcessLog("Please Press Load Button!");
    }

    private static final String ACTION = "android.wdvn.payment.entry";
    public static final String SECRET_KEY = "1111111111111111";//–For Testing
    public static final String KEY = "KEY";
    public static final String DATA = "DATA";
    public static final String RESULT = "RESULT";
    public static final int REQUEST_CODE = 102;//–3rd App self modify

    @OnClick(R.id.btn_load)
    public void onLoad() {
        BaseApplication.getProcessLog().setLength(0);
        BaseApplication.appendProcessLog("Please Press Load Button!");
        ivSuccess.setVisibility(View.GONE);
        ivFail.setVisibility(View.GONE);
        Constants.ip = edIp.getText().toString();
        Constants.port = Integer.parseInt(edPort.getText().toString());
        Constants.position = edPosition.getText().toString();
        if (Constants.ip == null) {
            BaseApplication.appendProcessLog("Please Input IP!");
            return;
        }
        if (Constants.port == 0) {
            BaseApplication.appendProcessLog("Please Input Port!");
            return;
        }
        if (!Constants.position.equals("")) {
            if ((Integer.parseInt(Constants.position) >= 1 && Integer.parseInt(Constants.position) <= 70)) {
                BaseApplication.appendProcessLog("IP: " + Constants.ip + " PORT:" + Constants.port + " POSITION: " + Constants.position);
                process();
            } else {
                BaseApplication.appendProcessLog("Please Input Position Between 1 - 70!");
            }
        } else {
            BaseApplication.appendProcessLog("Please Input Position!");
            return;
        }
    }

    @OnClick(R.id.btn_view_key)
    public void onViewKey() {
//        startActivity(new Intent(this, KeyActivity.class));
        //--Default will pop-up all payment application for user select.
        Intent intent = new Intent(ACTION);
//        intent.addCategory("com.wdvn.vcb"); //VCB , Open default VCB Payment App
        //intent.addCategory("com.wdvn.eib"); //EIB , Open default EIB Payment App
        intent.addCategory("com.wdvn.bidv"); //VTB , Open default VTB Payment App
        //…
        RequestData requestData = new RequestData();//–Define class to add Data
        requestData.tranxType = "SALE";
        requestData.transactionAmount = "20000";
//–....follow document to add more data by Transaction Type

//        String jsonData = "{\"initParams\":{\"refNo\":\"77770008\",\"ip\":\"14.161.16.120\",\"port\":\"10012\",\"enableEncrypt\":\"0\",\"nii\":\"008\",\"typeEncrypt\":\"BCD\"},\"merchant_transID\":\"20220513140717\",\"tranxType\":\"AUTO_INITIALIZE\"}";
        String jsonData = "{\"acqrID\":\"100\"," +
                "\"cardInfo\":{" +
                "\"CardHolderName\":\"PASSPORT PLUS\"," +
                "\"CardNumber\":\"9704037119750430\"," +
                "\"CardType\":\"S\",\n" +
                "\"ExpDate\":\"4911\",\n" +
                "\"ServiceCode\":\"521\",\n" +
                "\"Track1\":\"B9704037119750430^PASSPORT PLUS ^4911521000000000000000971000000\",\n" +
                "\"Track2\":\"9704037119750430=49115210000097199999\",\n" +
                "\"Track3\":\"\"\n" +
                "},\n" +
                "\"currencyName\":\"VND\",\"merchant_transID\":\"20220519164837\",\"tipAmount\":\"000000000000\",\"transactionAmount\":\"000002588000\",\"tranxType\":\"SALE\"}";

        jsonData = "{\"acqrID\":\"100\",\"AddInfo\":[{\"Content\":\" \"}],\"cardType\":0,\"currencyName\":\"VND\",\"merchant_transID\":\"20220607161536\",\"tipAmount\":\"0\",\"tranxType\":\"SALE\",\"transactionAmount\":\"000000005555\"}";
        intent.putExtra(KEY, SECRET_KEY);
        intent.putExtra("DATA", jsonData);
        startActivityForResult(intent, REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("Sang", "onActivityResult - resultCode: " + resultCode + " - data:" + data);
        if (data != null && data.getExtras() != null) {
            String json = data.getStringExtra("RESULT");
            if (json.contains("SALE")) {
                Log.d("Sang","json " + json);
                String checkTransaction = "{\"cardType\":0,\"merchant_transID\":\"20220607161233\",\"tranxType\":\"CHECK_TRANSACTION\"}";
                Intent intent = new Intent(ACTION);
//        intent.addCategory("com.wdvn.vcb"); //VCB , Open default VCB Payment App
                //intent.addCategory("com.wdvn.eib"); //EIB , Open default EIB Payment App
                intent.addCategory("com.wdvn.bidv"); //V
                intent.putExtra(KEY, SECRET_KEY);
                intent.putExtra("DATA", checkTransaction);
                startActivityForResult(intent, REQUEST_CODE);
            }
        }

    }

    @OnClick(R.id.iv_success)
    public void onSuccess() {
        reset();
        PaxApi.getInstance().shutdown();
//        Util.deleteIpFile();
        PaxApi.getInstance().removeApp(BuildConfig.APPLICATION_ID, new OnProcessApp() {
            @Override
            public void onProcessSuccess(int ret) {

            }

            @Override
            public void onProcessFail(int ret) {

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        reset();
    }

    private void reset() {
        if (mToastToShow != null) {
            mToastToShow.cancel();
        }
        if (toastCountDown != null) {
            toastCountDown.cancel();
        }
    }

    @OnClick(R.id.iv_fail)
    public void onFail() {
        client.closeConnection();
        BaseApplication.getProcessLog().setLength(0);
        BaseApplication.appendProcessLog("Please Press Load Button!");
        ivFail.setVisibility(View.GONE);
    }

    private void process() {
        client = new TCPClient();
        Single
                .create((SingleOnSubscribe<LogonResponse>) emitter -> {
                    BaseApplication.appendProcessLog("Logging on ...");
                    client.connectServer();
                    LogonRequest req = new LogonRequest();
                    req.serial = PaxApi.getInstance().getSerial();
                    PukInfo info = PaxApi.getInstance().getPUKInfo();
                    req.publicKey = Util.bytes2HexString(info.getPubKey()) + " " + Util.bytes2HexString(info.getSigInfo());
                    client.send(JsonUtils.getGson().toJson(req).getBytes(),
                            response -> {
                                BaseApplication.appendProcessLog("Logon Successfully");
                                LogonResponse rp = JsonUtils.getGson().fromJson((String) response, LogonResponse.class);
                                emitter.onSuccess(rp);
                            }, error -> {
                                emitter.onError((Throwable) error);
                            });
                })
                .flatMap((Function<LogonResponse, Single<KeyResponse>>) logonResponse -> getPUK(logonResponse.key))
                .flatMap((Function<KeyResponse, Single<Boolean>>) keyResponse -> writePUK(keyResponse))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if (response) {
                        SystemClock.sleep(500);
                        success();
                    } else {
                        BaseApplication.appendProcessLog("Write PUK Failed!!!");
                        ivFail.setVisibility(View.VISIBLE);
                    }
                    client.closeConnection();
                }, throwable -> {
                    BaseApplication.appendProcessLog("Error: " + throwable.getMessage());
                    throwable.printStackTrace();
                    client.closeConnection();
                    ivFail.setVisibility(View.VISIBLE);
                });

    }

    private void success() {
        PukInfo info = PaxApi.getInstance().getPUKInfo();
        String tmp = new String(info.getSigInfo());

        byte[] source = info.getSigInfo();
        byte[] target = new byte[15];
        System.arraycopy(source, 10, target, 0, target.length);
        tmp = new String(target);
        Log.d("Sang", "writePUKLoading " + BuildConfig.FLAVOR);
        // Request from Hiện for contain Payoo
        BaseApplication.appendProcessLog("Write PUK Successfully!!!");
        if (BuildConfig.time == 2) {
            if (tmp.contains("Paydi")) {
                showToast(tmp);
                ivSuccess.setVisibility(View.VISIBLE);
            } else {
                Constants.port = Constants.port + 1;
                process();
            }
        } else {
            showToast(tmp);
            ivSuccess.setVisibility(View.VISIBLE);
        }
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(edPort.getWindowToken(), 0);
        }
    }

    private Single<Boolean> writePUK(KeyResponse response) {
        return Single
                .create(new SingleOnSubscribe<Boolean>() {
                    @Override
                    public void subscribe(SingleEmitter<Boolean> emitter) throws Exception {
                        byte[] tmpPUK = EncryptUtils.decryptHexString3DES(response.puk, enKey.getBytes(), Constants.CIPHER_MODE, Constants.InitVector);
                        byte[] puk = new byte[response.size];
                        System.arraycopy(tmpPUK, 0, puk, 0, response.size);
                        BaseApplication.appendProcessLog("Check sum SHA-256 ...");
                        Log.d("Sang", "response.checkSum " + response.checkSum);
                        if (Util.getSha256Checksum(puk).equalsIgnoreCase(response.checkSum)) {
                            BaseApplication.appendProcessLog("Check sum SHA-256 correct");
                            BaseApplication.appendProcessLog("Writing PUK ...");
                            PaxApi.getInstance().writePUK((byte) 1, puk);

                            emitter.onSuccess(true);
                        } else {
                            BaseApplication.appendProcessLog("Check sum SHA-256 error\n" + response.checkSum + "\n" + Util.getSha256Checksum(puk));
                            emitter.onSuccess(false);
                        }
                    }
                });
    }

    private Single<KeyResponse> getPUK(String key) {
        int position = Integer.parseInt(Constants.position);
        enKey = key.substring(position, position + 24);
        return Single
                .create((SingleOnSubscribe<KeyResponse>) emitter -> {
                    BaseApplication.appendProcessLog("Getting PUK ...");
                    KeyRequest req = new KeyRequest();
                    req.serial = PaxApi.getInstance().getSerial();
                    String message = Constants.position;
                    int mod = message.length() % 8;
                    int padding = 0;
                    if (mod != 0) {
                        padding = 8 - mod;
                    }
                    byte[] plain = new byte[message.length() + padding];
                    System.arraycopy(message.getBytes(), 0, plain, 0, message.length());
                    req.position = new String(EncryptUtils.encrypt3DES2HexString(plain, enKey.getBytes(), Constants.CIPHER_MODE, Constants.InitVector));
                    client.send(JsonUtils.getGson().toJson(req).getBytes(),
                            response -> {
                                KeyResponse rp = JsonUtils.getGson().fromJson((String) response, KeyResponse.class);
                                BaseApplication.appendProcessLog("Get PUK Successfully");
                                emitter.onSuccess(rp);
                            }, error -> {
                                emitter.onError((Throwable) error);
                            });
                });
    }

    private Toast mToastToShow;
    private CountDownTimer toastCountDown;

    private void showToast(String owner) {
        // Set the toast and duration
        int toastDurationInMilliSeconds = 3000000;
        mToastToShow = Toast.makeText(this, "Owner: " + owner, Toast.LENGTH_LONG);
        TextView textView = new TextView(this);
        textView.setTextColor(Color.RED);
        textView.setText("Sang");
//        mToastToShow.setView(textView);
        mToastToShow.setGravity(Gravity.CENTER, 0, 0);
        // Set the countdown to display the toast
        toastCountDown = new CountDownTimer(toastDurationInMilliSeconds, 1000 /*Tick duration*/) {
            public void onTick(long millisUntilFinished) {
                mToastToShow.show();
            }

            public void onFinish() {
                mToastToShow.cancel();
            }
        };

        // Show the toast and starts the countdown
        mToastToShow.show();
        toastCountDown.start();
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_PERMISSION);
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case REQUEST_PERMISSION: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    // call your method
                    readInfoFromFile();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}