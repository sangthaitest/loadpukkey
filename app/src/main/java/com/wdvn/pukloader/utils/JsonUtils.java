package com.wdvn.pukloader.utils;

import com.google.gson.Gson;

public class JsonUtils {

    public static final String TAG = JsonUtils.class.getSimpleName();

    private static Gson instance;

    public static Gson getGson() {
        if (instance == null) {
            return new Gson();
        }
        return instance;
    }

}
