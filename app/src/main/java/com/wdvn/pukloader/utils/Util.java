package com.wdvn.pukloader.utils;

import android.os.Environment;
import android.util.Log;

import com.wdvn.pukloader.BuildConfig;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

public class Util {
    public static final String FILE_DECRYPT = "ipconfig.json";
    public static final String TAG = Util.class.getSimpleName();

    private static Util instance;

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    public static Util getInstance() {
        if (instance == null) {
            return new Util();
        }
        return instance;
    }

    public byte[] readFile(String path) {
        byte[] result = null;
        File f;
        FileInputStream fis;
        try {
            f = new File(path);
            result = new byte[(int) f.length()];
            fis = new FileInputStream(f);
            fis.read(result);
            fis.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public static String bytes2HexString(final byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static byte[] hexString2Bytes(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static String getSha256Checksum(byte[] bytes) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] hash = digest.digest(bytes);
        return bytes2HexString(hash);
    }

    public static String readFromFile() {
        File data = Environment.getDataDirectory();
        String currentDBPath = "/data/" + BuildConfig.APPLICATION_ID + "/files/" + FILE_DECRYPT + "";
        String aBuffer = "";
        try {
            File myFile = new File(data, currentDBPath);
            Log.d("Sang", "myFile: " + myFile);
            if (myFile.exists()) {

                FileInputStream fIn = new FileInputStream(myFile);
                BufferedReader myReader = new BufferedReader(new InputStreamReader(fIn));
                String aDataRow = "";
                while ((aDataRow = myReader.readLine()) != null) {
                    aBuffer += aDataRow;
                }
                myReader.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return aBuffer;
    }

    public static boolean deleteIpFile() {
        File myFile = new File(Environment.getExternalStorageDirectory() + "/" + FILE_DECRYPT);
        return myFile.exists() && myFile.delete();
    }
}
