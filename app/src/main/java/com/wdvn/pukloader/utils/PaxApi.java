package com.wdvn.pukloader.utils;

import com.pax.dal.IDAL;
import com.pax.dal.entity.ETermInfoKey;
import com.pax.dal.entity.PukInfo;
import com.pax.dal.exceptions.PukDevException;
import com.pax.neptunelite.api.NeptuneLiteUser;
import com.wdvn.pukloader.base.BaseApplication;
import com.wdvn.pukloader.interfaces.OnProcessApp;

public class PaxApi {

    public static final String TAG = PaxApi.class.getSimpleName();

    private static PaxApi instance;
    private IDAL idal;

    private PaxApi() {
        try {
            idal = NeptuneLiteUser.getInstance().getDal(BaseApplication.getApp());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static PaxApi getInstance() {
        if (instance == null) {
            return new PaxApi();
        }
        return instance;
    }

    public void writePUK(byte b, byte[] key) throws PukDevException {
        idal.getPuk().writePuk(b, key);
    }

    public PukInfo getPUKInfo() {
        try {
            return idal.getPuk().readPuk((byte) 1);
        } catch (PukDevException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void removeApp(String appId, OnProcessApp callback) {
        int ret = idal.getSys().uninstallApp(appId);
        if (ret == 0) {
            callback.onProcessSuccess(ret);
        } else {
            callback.onProcessFail(ret);
        }
    }

    public void shutdown() {
        idal.getSys().shutdown();
    }

    public String getSerial() {
        return idal.getSys().getTermInfo().get(ETermInfoKey.SN);
    }

}
