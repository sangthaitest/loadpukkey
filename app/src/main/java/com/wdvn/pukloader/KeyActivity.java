package com.wdvn.pukloader;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.pax.dal.entity.PukInfo;
import com.wdvn.pukloader.utils.PaxApi;
import com.wdvn.pukloader.utils.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.android.material.snackbar.Snackbar;

public class KeyActivity extends AppCompatActivity {

    public static final String TAG = KeyActivity.class.getSimpleName();

    @BindView(R.id.tv_pub_key)
    TextView tvPubKey;
    @BindView(R.id.tv_sign_info)
    TextView tvSignInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_key);
        ButterKnife.bind(this);

        PukInfo info = PaxApi.getInstance().getPUKInfo();
        tvPubKey.setText(Util.bytes2HexString(info.getPubKey()));
        tvSignInfo.setText(Util.bytes2HexString(info.getSigInfo()));
    }
}