package com.wdvn.pukloader.base;

import android.util.Log;

import androidx.multidex.MultiDexApplication;

import com.pax.dal.IDAL;
import com.pax.neptunelite.api.NeptuneLiteUser;
import com.wdvn.pukloader.utils.PaxApi;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public class BaseApplication extends MultiDexApplication {

    public static final String TAG = BaseApplication.class.getSimpleName();

    private static BaseApplication mApp;
    private static IDAL dal;

    private static StringBuilder processLog = new StringBuilder();
    public static Subject<String> mProcessObservable = PublishSubject.create();

    @Override
    public void onCreate() {
        super.onCreate();
        
        init();
    }

    private void init() {
        mApp = this;
        try {
            dal = NeptuneLiteUser.getInstance().getDal(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static BaseApplication getApp() {
        return mApp;
    }

    public static IDAL getDal() {
        return dal;
    }

    public static Observable getProcessLogObs() {
        return mProcessObservable;
    }

    public static void appendProcessLog(String msg) {
        processLog = processLog.append("* " +msg + "\n");
        mProcessObservable.onNext(processLog.toString());
        Log.e(TAG, msg);
    }

    public static StringBuilder getProcessLog() {
        return processLog;
    }
}
