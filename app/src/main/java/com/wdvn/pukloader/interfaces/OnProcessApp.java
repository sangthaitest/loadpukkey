package com.wdvn.pukloader.interfaces;

public interface OnProcessApp {

    public void onProcessSuccess(int ret);

    public void onProcessFail(int ret);

}
