package com.wdvn.pukloader.api;

import android.util.Log;

import androidx.core.util.Consumer;

import com.wdvn.pukloader.utils.Constants;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.channels.Channel;

public class TCPClient {

    public static final String TAG = TCPClient.class.getSimpleName();

    // create Socket object
    private Socket client;
    private String serverHost = "10.247.42.202";
    private int serverPort = 45678;
    //    private String serverHost = "10.247.42.29";
//    private int serverPort = 34567;
    private InputStream is = null;
    private OutputStream os = null;
    private Channel channel;

    public void connectServer() {
        try {
            client = new Socket(Constants.ip, Constants.port);
            Log.e(TAG, "connectServer: ");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public void send(byte[] data, Consumer response, Consumer error) {
        try {
            is = client.getInputStream();
            os = client.getOutputStream();
            os.write(data);
            os.flush();
            byte[] recv = new byte[1024 * 2];
            int total = is.read(recv);
            byte[] value = new byte[total];
            System.arraycopy(recv, 0, value, 0, total);
            response.accept(new String(value));
        } catch (IOException e) {
            e.printStackTrace();
            error.accept(e);
        } finally {
//            closeStream(is, error);
//            closeStream(os, error);
//            closeSocket(client, error);
        }
    }

    public void closeConnection() {
        closeStream(is);
        closeStream(os);
        closeSocket(client);
    }

    public void closeSocket(Socket socket) {
        try {
            if (socket != null) {
                socket.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void closeStream(InputStream inputStream) {
        try {
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void closeStream(OutputStream outputStream) {
        try {
            if (outputStream != null) {
                outputStream.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
