package com.wdvn.pukloader.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PResponse {

    @SerializedName("Serial")
    @Expose
    public String serial;

}
