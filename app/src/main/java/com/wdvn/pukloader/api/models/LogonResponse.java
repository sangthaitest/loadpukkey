package com.wdvn.pukloader.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LogonResponse extends PResponse {

    @SerializedName("Key")
    @Expose
    public String key;

}
