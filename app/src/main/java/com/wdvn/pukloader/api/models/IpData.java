package com.wdvn.pukloader.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IpData {

    @SerializedName("IP")
    @Expose
    public String ip;

    @SerializedName("Port")
    @Expose
    public int port;

    @SerializedName("Position")
    @Expose
    public int pos;

}
