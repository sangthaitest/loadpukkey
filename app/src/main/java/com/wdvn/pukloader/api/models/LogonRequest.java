package com.wdvn.pukloader.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LogonRequest extends PRequest{

    @SerializedName("PublicKey")
    @Expose
    public String publicKey;

}
