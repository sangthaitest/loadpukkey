package com.wdvn.pukloader.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KeyResponse extends PResponse {

    @SerializedName("CheckSum")
    @Expose
    public String checkSum;

    @SerializedName("PUK")
    @Expose
    public String puk;

    @SerializedName("Size")
    @Expose
    public int size;

}
